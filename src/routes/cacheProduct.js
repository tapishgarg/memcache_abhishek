const express=require('express')
const controller=require('../controllers/employeesController')
const memcached=require('../middlewares/memcachedMiddleware')

const router=express.Router()

router.get('/', controller.addOrEditController)

router.get('/search', controller.searchController)

router.post('/', controller.insertOrUpdateController)

router.get('/list', memcached.memcachedMiddleware(1), controller.listController)

router.get('/:id', controller.getByIdController)

router.get('/delete/:id', controller.deleteByIdController)

module.exports= router;


