require('./models/db')

const express =require('express')
const path = require('path')
const exphbs = require('express-handlebars')
const bodyparser = require('body-parser')
var responseTime = require('response-time')
const RegisterRoutes= require('./routes')


var app = express()
//Instead of passing in url pass as body which will later on get convert in json
app.use(bodyparser.urlencoded({
    extended:true
}))

//Body into json
app.use(bodyparser.json())

app.set('views',path.join(__dirname,'/views'))
//Handlebar configuration
app.engine('hbs',exphbs({extname:'hbs',defaultLayout:'mainLayout', layoutsDir:__dirname + '/views/layout'}))
app.set('view engine','hbs')

RegisterRoutes(app);

app.get("/", (req, res) => res.sendFile(__dirname + "/views/layout"));

/**
 * Error Handling
 */
app.use(function(error, req, res, next) 
{
    let err = { code: 404, error: error };
    res.status(404).send(err);
});


// const employeeController = require('./controllers/employeeController')

// app.listen(3004,() =>{
//     console.log('Express Server started at port : 3004')
// }
// )
app.use(responseTime())

module.exports=app

// app.use('/employee' , employeeController)
