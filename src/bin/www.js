const dotenv= require('dotenv')

let envPath= ".env." + process.env.ENV
dotenv.config({path: envPath})

const app= require('../server')
const http= require('http')

const normalizePort=(val)=>{
    const port= parseInt(val, 10)

    if(isNaN(port)){
        return val
    }
    if(port>=0){
        return port
    }
    return false
}

const onError=(error)=>{
    if(error.syscall !== 'listen'){
        throw error
    }

    const bind= typeof port === "string" ? "Pipe"+port :"Port"+port

    switch(error.code){
        case "EACCES": 
            console.error(bind + "requires elevated privileges")
            process.exit(1)
            break
        case "EADDRINUSE": 
            console.error(bind+ "is aleady in use")
            process.exit(1)
            break
        default: 
            throw error
    }
}

const onListening=()=>{
    const address= server.address()
    const bind= typeof port=== "string"? "Pipe"+port: "Port "+port
    if(process.env.ENV==="dev"){
        console.log( "Listening on "+ bind)
    }
}

const port=normalizePort(process.env.PORT || 3004)
app.set('port',port)

const server=http.createServer(app)

server.listen(port)
server.on("error", onError)
server.on("listening", onListening)



