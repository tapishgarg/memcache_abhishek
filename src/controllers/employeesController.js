const mongoose=require('mongoose')
const Employee= mongoose.model('Employee')

module.exports.addOrEditController= (req,res)=>{
    res.render('employee/addOrEdit',{
        viewTitle: "Insert Employee"
    })
}

module.exports.searchController= (req,res)=>{
    res.render("employee/search",{
        viewTitle:"Search Employee"
    })
}

module.exports.insertOrUpdateController=(req,res)=>{
    if(req.body._id=='')
    insertRecord(req,res)
    else
    updateRecord(req,res)   
}

module.exports.listController=(req,res)=>{
    Employee.find((err,docs) => {
        if(!err){
            res.render('employee/list',{
                list: docs
            })
        }
        else{
            console.log('Error in retriving employee list' + err)
        }
    })
}

module.exports.getByIdController=(req,res)=>{
    Employee.findById(req.params.id,(err,docs) => {
        if(!err){
            res.render("employee/addOrEdit",{
                viewTitle:"Update Employee",
                employee:docs
            })
        }
        else{
            console.log('Error in retriving employee list' + err)
        }
    })
}

module.exports.deleteByIdController=(req,res)=>{
    Employee.findByIdAndDelete(req.params.id,(err,docs) => {
        if(!err){
            res.redirect('/employee/list')
        }
        else{
            console.log('Error in deleting employee' + err)
        }
    })
}

//Functions

//Validating the user input
function handleValidationError(err,body){
    for(field in err.errors){
        switch(err.errors[field].path){
            case 'fullname':
                body['fullNameError'] = err.errors[field].message
                break
            case 'email':
                body['emailError'] =err.errors[field].message
                break
            default:
                break

        }
    }
}


//Insert record into Mongodb
function insertRecord(req,res){
    var employee = new Employee()
    employee.fullname=req.body.fullname
    employee.email=req.body.email
    employee.mobile=req.body.mobile
    employee.city=req.body.city
    employee.save((err,doc)=>{
        if(!err){
            res.redirect('employee/list')
        }
        else{
            if(err.name == 'ValidationError'){
                handleValidationError(err,req.body)
                res.render("employee/addOrEdit",{
                    viewTitle:"Insert Employee",
                    employee:req.body
                })
            }     
            else
            console.log("Error during record insertion : " + err)
        }
    })

}

function updateRecord(req,res){
    Employee.findByIdAndUpdate({_id:req.body._id},req.body,{new:true},(err,doc) => {
        if(!err){
            res.redirect('employee/list')
        }
        else{
            if(err.name == 'ValidationError'){
                handleValidationError(err,req.body)
                res.render("employee/addOrEdit",{
                    viewTitle:"Update Employee",
                    employee:req.body
                })
            }     
            else
            console.log("Error during record Updation : " + err)
        }
    })
}
