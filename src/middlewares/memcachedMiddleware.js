const Memcached = require('memcached')
var memcached = new Memcached("127.0.0.1:11211")

module.exports.memcachedMiddleware = (duration) => {
    return  (req, res, next) => {
      let key = "__express__" + req.originalUrl || req.url;
  
      memcached.get(key, function(err, data) {
        if (data) {
          res.send(data);
          console.log("ready")
          return;
        } else {
          res.sendResponse = res.send;
          
          res.send = (body) => {
            console.log("Cached")
          memcached.set(key, body, (duration*60), function(err) { // duration*60 is 1 minute
            console.log(err, '$$$$$$$$$$$$$$$$');
          });

          res.sendResponse(body);
        }

          res.send = (body) => {
              console.log("Replaced")
            memcached.replace(key, body, (duration*60), function(err) { // duration*60 is 1 minute
              console.log(err, '%%%%%%%%%%%%%%%%%%');
            });
  
            res.sendResponse(body);
          }
          next();
        }
      });
    }
};

// module.exports.deleteMemcachedMiddleware=()=>{
//   return (req,res,next)=>{
//     let key = "__express__" + req.originalUrl || req.url;
    
//     memcached.delete(key, (err,data)=>{
//       if(!data){
//         console.log("Data deleted")
//       }
//     })
//   }
// }