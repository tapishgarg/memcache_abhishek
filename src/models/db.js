const mongoose = require('mongoose')
let con = `${process.env.MONGOHOST}/${process.env.MONGODATABASE}`
mongoose.connect(con,{useNewUrlParser:true},(err) => {

// mongoose.connect('mongodb://localhost:27017/EmployeeDB',{useNewUrlParser:true},(err) => {
    if(!err){ 
        console.log("Mongodb connected Successfully!", con)
    }
    else{
        console.log("Error while connecting!")
    }
})

require('./employee_model')
